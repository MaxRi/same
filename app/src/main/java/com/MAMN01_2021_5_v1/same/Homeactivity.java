package com.MAMN01_2021_5_v1.same;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

public class Homeactivity extends AppCompatActivity {

    TextView longitud;
    TextView latitud;
    private SensorManager sensorManager;
    private Sensor gpsSensor;
    private LocationManager locationManager = null;
    private LocationListener locationListener = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homeactivity);

        longitud = (TextView) findViewById(R.id.textView2);
        latitud = (TextView) findViewById(R.id.textView3);
        longitud.setText("Longitude: ");
        latitud.setText("Latitude: ");

        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();




        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }



        locationManager.requestLocationUpdates(LocationManager
                .GPS_PROVIDER, 5000, 1, locationListener);
    }

    public void goToFindTreasure(View view) {
        Intent intent = new Intent(this, findTreasureActivity.class);
        startActivity(intent);
    }

    public void goToDigTreausre(View view) {
        Intent intent = new Intent(this, digTreasureActivity.class);
        startActivity(intent);
    }
    public void goToAdmin(View view) {
        Intent intent = new Intent(this, adminActivity.class);
        startActivity(intent);
    }

    /*----Method to Check GPS is enable or disable ----- */
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    /*----------Listener class to get coordinates ------------- */
    private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {


            String longitude = "Longitude: " + loc.getLongitude();
            String latitude = "Latitude: " + loc.getLatitude();
            latitud.setText(latitude);
            longitud.setText(longitude);
        }

    }

}